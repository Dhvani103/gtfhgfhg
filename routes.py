from app import app
from flask import jsonify
import os

@app.route("/authentication", methods=["GET"])
def index():
    return "Welcome Dhruv Desai! You are authenticated to use the API."

@app.route("/mongodbcluster", methods=["GET"])
def mongodb_db_client():
    username = os.environ.get("USERNAME","mongodb_test_user")
    password = os.environ.get("PASSWORD","mongodb_test_password")
    cluster_url = os.environ.get("CLUSTER_URL","mongodb_test_password:mongodb_test_password@127.0.0.1:27017/")
    data = os.environ.get("DATA","age: 50, height: 182, Weight: 85")


    mock_data = {
        "USERNAME":username,
        "PASSWORD": password,
        "CLUSTER_URL": cluster_url,
        "DATA": data
    }

    return jsonify(mock_data)
